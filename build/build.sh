#!/bin/sh

phpversion=$1

PHP_VERSION=${phpversion:-8.3}
IMG_TAG=$PHP_VERSION

docker build \
    --build-arg PHP_VERSION=$PHP_VERSION \
    --no-cache \
    --tag dockerwest/php-magento1:$PHP_VERSION \
    .
